#============================================================================
# Created    : 2008-03-06
# Maintainer : Patrick ROBBE, Gloria CORTI
#============================================================================
package GENSER
version v17r0

# Structure, i.e. directories to process. 
#============================================================================
branches doc cmt src

# Used packages
#============================================================================

use GaudiPolicy  v*

#-- LHAPDF
#============================================================================
macro lhapdf_native_version "6.1.4"
use lhapdf     v* LCG_GeneratorsInterfaces
use LHAPDFSets v*

#-- Herwig++ and ThePeg
# Set native versions of required software.
#============================================================================
macro herwigpp_native_version "2.7.1"
macro thepeg_native_version   "1.9.2p1"


#-- AlpGen
#============================================================================
macro alpgen_native_version "2.1.4"


#-- Powheg-box
#============================================================================
macro powhegbox_native_version "r3043.lhcb"

#-- Photos++ and Taula++ 
#============================================================================
macro photospp_native_version "3.56"
macro tauolapp_native_version "1.1.6"


#-- Pythia 8
#   PYTHIA8DATA is needed by EvtGen, -lpythia8tohepmc for LbPythia8
#============================================================================
macro pythia8_native_version "219"
#macro_append pythia8_linkopts " -lpythia8tohepmc " # it is no longer in 219
set PYTHIA8DATA $(pythia8_home)/share/Pythia8/xmldoc

#-- Pythia 6
#============================================================================
macro pythia6_native_version "427.2"
use pythia6  v* LCG_GeneratorsInterfaces -no_auto_imports
macro_remove pythia6_linkopts " -lpythia6_pdfdummy " \
                        WIN32 " pythia6_pdfdummy.lib "

#-- Rivet for reference analysis with comparison with published data and
#-- Professor for tuning, also needs Yoda for Rivet 2.0
#============================================================================
macro rivet_native_version "2.4.2"
macro yoda_native_version "1.5.9"


#-- Redefine symbols of MCGenerators libraries
#-- Pythia 6
#============================================================================
library      pythia6forgauss   pythia6/*.F

apply_pattern linker_library library=pythia6forgauss
macro_append pythia6forgauss_shlibflags "$(pythia6_home)/lib/pydata.o -lnsl -lcrypt -ldl -lg2c" \
             target-gcc4 "$(pythia6_home)/lib/pydata.o -lnsl -lcrypt -ldl"

## FIXME: Move to include of install area
##apply_pattern install_more_includes more=GENSER
include_dirs $(GENSERROOT)/src/pythia6   

# force the order of link for pythia6
macro_append GENSER_linkopts " $(pythia6_linkopts) "

# same for EPOS
library      eposforgauss   epos/*.F

apply_pattern linker_library library=eposforgauss

# force the order of link for epos
macro_append GENSER_linkopts " $(epos_linkopts) "


#-- Hijing
#============================================================================
macro hijing_native_version "1.383bs.2"

#-- CRMC/EPOS
#============================================================================
macro crmc_native_version "1.5.6"

#-- linkopts
#============================================================================
private
macro_remove libraryshr_linkopts "-Wl,--no-undefined"

public
macro GENSER_linkopts "-lnsl -lcrypt -ldl -lg2c" \
      target-gcc4     "-lnsl -lcrypt -ldl -lgfortran" \
      WIN32           "/LIBPATH:$(DFDir)\LIB DFORDLL.LIB DFPORT.LIB"


macro_prepend GENSER_linkopts " -Wl,--no-as-needed " \
              WIN32           ""
macro_append  GENSER_linkopts " -Wl,--as-needed " \
              WIN32           ""

macro_remove fflags "" \
      target-gcc4   "-ff90"

macro_append fflags "" \
      target-gcc4   " -fsecond-underscore "

# do not build on windows
#============================================================================
apply_pattern disable_package_on platform=target-winxp

